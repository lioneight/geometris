#import "GeometrisPlugin.h"

@implementation GeometrisPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"lioneight.geometris"
            binaryMessenger:[registrar messenger]];
  GeometrisPlugin* instance = [[GeometrisPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"getPlatformVersion" isEqualToString:call.method]) {
    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
  } else if ([@"wqaInitialize" isEqualToString:call.method]) {
  
  } else if ([@"wherequbeServiceInitialize" isEqualToString:call.method]) {
   
  } else if ([@"wherequbeServiceDestroy" isEqualToString:call.method]) {
    
  } else if ([@"wherequbeServiceIsConnected" isEqualToString:call.method]) {
     
  } else if ([@"wherequbeServiceConnect" isEqualToString:call.method]) {
       
  } else if ([@"wherequbeServiceDisconnect" isEqualToString:call.method]) {
        
  } else if ([@"wherequbeServiceScanDevices" isEqualToString:call.method]) {

  } else if ([@"wherequbeServiceStartTransmittingUnidentifiedDriverMessages" isEqualToString:call.method]) {

  } else if ([@"wherequbeServiceStopTransmittingUnidentifiedDriverMessages" isEqualToString:call.method]) {

  } else if ([@"wherequbeServicePurgeUnidentifiedDriverMessages" isEqualToString:call.method]) {

  } else {
    result(FlutterMethodNotImplemented);
  }
}

@end

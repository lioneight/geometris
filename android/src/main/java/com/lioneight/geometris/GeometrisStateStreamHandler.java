package com.lioneight.geometris;

import android.app.Activity;

import com.geometris.wqlib.AbstractWherequbeStateObserver;
import com.geometris.wqlib.WQError;

import java.util.ArrayList;
import java.util.List;

import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.PluginRegistry.Registrar;

public class GeometrisStateStreamHandler implements EventChannel.StreamHandler {

    private static final String TAG = "GeometrisStateStreamHandler";

    private static final String WherequbeStateCONNECTED = "CONNECTED";
    private static final  String WherequbeStateSYNCED = "SYNCED";
    private static final  String WherequbeStateDISCOVERED = "DISCOVERED";
    private static final  String WherequbeStateERROR = "ERROR";
    private static final  String WherequbeStateDISCONNECTED = "DISCONNECTED";

    public static final String NAME = "lioneight.geometris/sensors/wherequbeState";

    private final Activity activity;

    public GeometrisStateStreamHandler(Activity activity) {
        this.activity = activity;
    }

    private AbstractWherequbeStateObserver wherequbeStateObserver;

    @Override
    public void onListen(Object arguments, EventChannel.EventSink events) {
        wherequbeStateObserver = createWherequbeStateObserver(events);
        wherequbeStateObserver.register(activity);
    }

    @Override
    public void onCancel(Object o) {
        if(wherequbeStateObserver != null){
            wherequbeStateObserver.unregister(activity);
        }
    }

    private AbstractWherequbeStateObserver createWherequbeStateObserver(final EventChannel.EventSink events) {
        return new AbstractWherequbeStateObserver() {

            @Override
            public  void onConnected() {
                activity.runOnUiThread(() -> events.success(WherequbeStateCONNECTED)
                );
            }

            @Override
            public void onSynced() {
                activity.runOnUiThread(() -> events.success(WherequbeStateSYNCED)
                );
            }

            @Override
            public void onDiscovered() {
                activity.runOnUiThread(() -> events.success(WherequbeStateDISCOVERED));
            }

            @Override
            public  void onDisconnected()
            {
                activity.runOnUiThread(() -> events.success(WherequbeStateDISCONNECTED));
            }

            @Override
            public void onError(final WQError var1) {
                activity.runOnUiThread(() -> events.success(WherequbeStateERROR + ","  + var1.mCode));
            }
        };
    }
}

package com.lioneight.geometris;

import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** GeometrisPlugin */
public class GeometrisPlugin {

  /** Plugin registration. */
  public static void registerWith(Registrar registrar) {

    final MethodChannel channel = new MethodChannel(registrar.messenger(), GeometrisEventHandler.NAME);
    channel.setMethodCallHandler(new GeometrisEventHandler(registrar.activity()));

    final EventChannel stateChannel =
            new EventChannel(registrar.messenger(), GeometrisStateStreamHandler.NAME);
    stateChannel.setStreamHandler(new GeometrisStateStreamHandler(registrar.activity()));

    final EventChannel obdChannel =
            new EventChannel(registrar.messenger(), GeometrisObdMeasurementStreamHandler.NAME);
    obdChannel.setStreamHandler(new GeometrisObdMeasurementStreamHandler(registrar.activity()));
  }
}

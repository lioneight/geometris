package com.lioneight.geometris;

import android.app.Activity;
import android.content.Context;
import android.util.JsonWriter;

import androidx.annotation.NonNull;

import com.geometris.wqlib.BaseRequest;
import com.geometris.wqlib.BaseResponse;
import com.geometris.wqlib.GeoData;
import com.geometris.wqlib.GetDeviceAddress;
import com.geometris.wqlib.RequestHandler;
import com.geometris.wqlib.ResponseHandler;
import com.geometris.wqlib.UnidentifiedEvent;
import com.geometris.wqlib.WherequbeService;

import java.io.StringWriter;

import io.flutter.plugin.common.EventChannel;

import android.util.Log;

public class GeometrisObdMeasurementStreamHandler implements EventChannel.StreamHandler {

  private static final String TAG = "GeometStateStreamHndlr";

  public static final String NAME = "lioneight.geometris/sensors/obdMeasurement";

  private final Activity activity;

  public GeometrisObdMeasurementStreamHandler(Activity activity) {
    this.activity = activity;
  }

  EventChannel.EventSink events;

  RequestHandler eventHandler;

  @Override
  public void onListen(Object arguments, EventChannel.EventSink events) {
    Log.d(TAG, "onListen started");
    this.events = events;
    this.eventHandler = createEventHandler();

    WherequbeService.getInstance().setReqHandler(BaseRequest.OBD_MEASUREMENT, eventHandler);
    WherequbeService.getInstance().setReqHandler(BaseRequest.REQUEST_DEVICE_ADDRESS, eventHandler);
    WherequbeService.getInstance().setReqHandler(BaseRequest.REQUEST_START_UDEVENTS, eventHandler);

    ResponseHandler addressResponseHandler = createEventResponseHandler();

    WherequbeService.getInstance().sendRequest(new GetDeviceAddress(), addressResponseHandler, 50000);

    Log.d(TAG, "onListen finished");
  }

  @Override
  public void onCancel(Object o) {
    events = null;
  }

  private RequestHandler createEventHandler() {
    return new RequestHandler() {
      @Override
      public void onRecv(Context context, BaseRequest request) {
        if (events != null && request != null && request.requestId == BaseRequest.OBD_MEASUREMENT) {

          GeoData geoData = (GeoData) request.getObject();
          if (geoData != null) ;
          final String response = geoDataToJson(geoData);
          if (response != null) {
            activity.runOnUiThread(() -> {events.success(response);});
          }
        }
      }
    };
  }

  private ResponseHandler createEventResponseHandler(){
    return new ResponseHandler() {
      @Override
      public void onRecv(@NonNull Context var1, @NonNull BaseResponse var2) {
        activity.runOnUiThread(() -> {events.success("data received");});
      }

      @Override
      public void onError(@NonNull Context var1) {
        activity.runOnUiThread(() -> {events.success("on error received");});
      }
    };
  }

  static String geoDataToJson(GeoData geoData){
    StringWriter out = new StringWriter();
    JsonWriter writer = new JsonWriter(out);
    try {
      writer.beginObject();
      writer.name("protocolId").value(geoData.getProtocol());
      writer.name("vin").value(geoData.getVin());
      writer.name("odometer").value(geoData.getOdometer());
      writer.name("odometerTimeStamp").value(geoData.getOdometerTimestamp() != null ? geoData.getOdometerTimestamp().getMillis() : null);
      writer.name("engTotalHours").value(geoData.getEngTotalHours());
      writer.name("engTotalHoursTimestamp").value(geoData.getEngTotalHoursTimestamp() != null ? geoData.getEngTotalHoursTimestamp().getMillis() : null);
      writer.name("vehicleSpeed").value(geoData.getVehicleSpeed());
      writer.name("vehicleSpeedTimestamp").value(geoData.getVehicleSpeedTimestamp() != null ? geoData.getVehicleSpeedTimestamp().getMillis() : null);
      writer.name("engineRpm").value(geoData.getEngineRPM());
      writer.name("engineRpmTimestamp").value(geoData.getEngineRpmTimestamp() != null ? geoData.getEngineRpmTimestamp().getMillis() : null);
      writer.name("latitude").value(geoData.getLatitude());
      writer.name("longitude").value(geoData.getLongitude());
      writer.name("gpsTime").value(geoData.getGpsTime());
      writer.name("timeStamp").value(geoData.getTimeStamp() != null ? geoData.getTimeStamp().getMillis() : null);
      writer.name("totalUdrvEvents").value(geoData.getTotalUdrvEvents());
      writer.name("dataSet").value(geoData.isDataSet());

      writer.name("unidentifiedEventArrayList");
      writer.beginArray();
      for(UnidentifiedEvent udEvent : geoData.getUnidentifiedEventArrayList()){
        writer.beginObject();
        writer.name("reason").value(udEvent.getReason());
        writer.name("timestamp").value(udEvent.getTimestamp());
        writer.name("engTotalHours").value(udEvent.getEngTotalHours());
        writer.name("vehicleSpeed").value(udEvent.getVehicleSpeed());
        writer.name("odometer").value(udEvent.getOdometer());
        writer.name("latitude").value(udEvent.getLatitude());
        writer.name("longitude").value(udEvent.getLongitude());
        writer.name("gpsTimeStamp").value(udEvent.getGPSTimestamp());
        writer.endObject();
      }
      writer.endArray();

      writer.endObject();
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

    return out.toString();

  }
}

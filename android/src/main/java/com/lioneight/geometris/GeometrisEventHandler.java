package com.lioneight.geometris;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.geometris.wqlib.WQScanner;
import com.geometris.wqlib.Whereqube;
import com.geometris.wqlib.WherequbeService;
import com.geometris.wqlib.Wqa;

import java.util.ArrayList;
import java.util.List;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

public class GeometrisEventHandler implements MethodCallHandler {

  private static final String TAG = "GeometrisPlugin";

  private final Activity activity;

  public static final String NAME = "lioneight.geometris";

  public GeometrisEventHandler(Activity activity) {
    this.activity = activity;
  }

  private WQScanner mScanner;

    @Override
    public void onMethodCall(MethodCall call, @NonNull final Result result) {
        switch (call.method) {
            case "wqaInitialize":
                Wqa.getInstance().initialize(activity);
                result.success(true);
                break;
            case "wherequbeServiceInitialize":
                WherequbeService.getInstance().initialize(activity);
                result.success(true);
                break;
            case "wherequbeServiceDestroy":
                WherequbeService.getInstance().destroy(activity);
                result.success(true);
                break;
            case "wherequbeServiceIsConnected":
                boolean connected = WherequbeService.getInstance().isConnected();
                result.success(connected);
                break;
            case "wherequbeServiceConnect":
                String address = call.argument("address");
                boolean deviceConnected = WherequbeService.getInstance().connect(address);
                result.success(deviceConnected);
                break;
            case "wherequbeServiceDisconnect":
                WherequbeService.getInstance().close();
                result.success(true);
                break;
            case "wherequbeServiceScanDevices":
                scanDevices(result);
                break;
            case "wherequbeServiceStartTransmittingUnidentifiedDriverMessages":
                WherequbeService.getInstance().startTransmittingUnidentifiedDriverMessages();
                result.success(true);
                break;
            case "wherequbeServiceStopTransmittingUnidentifiedDriverMessages":
                WherequbeService.getInstance().stopTransmittingUnidentifiedDriverMessages();
                result.success(true);
                break;
            case "wherequbeServicePurgeUnidentifiedDriverMessages":
                WherequbeService.getInstance().purgeUnidentifiedDriverMessages();
                result.success(true);
                break;
            default:
                result.notImplemented();
                break;
        }
    }

  private void scanDevices(@NonNull final Result result) {
    if(mScanner != null){
      Log.i(TAG, "Whereqube scan already in progress ");
      result.success("");
      return;
    }

    final BluetoothManager bluetoothManager =
            (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
    BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
    // Checks if Bluetooth is supported on the device.
    if (mBluetoothAdapter == null) {
      Log.i(TAG, "Whereqube ble not supported");
      result.success("");
      return;
    }

      WQScanner.ScanResultListener scanResultListener = new WQScanner.ScanResultListener() {
          @Override
          public void onScanCompleted(@NonNull final List<Whereqube> wqubes) {
              List<String> data = new ArrayList<>();

              try {
                  Log.i(TAG, "Whereqube scanned " + wqubes.size());
                  for (Whereqube wqube : wqubes) {
                      data.add(wqube.mDevice.getAddress());
                      data.add(String.valueOf(wqube.mDevice.getName()));
                      data.add(String.valueOf(wqube.mRssi));
                  }
              } finally {
                  mScanner.stop();
                  mScanner = null;
                  result.success(join(",", data));
              }
          }
      };
    mScanner = new WQScanner(scanResultListener);
    long scanDuration = 30000;
    mScanner.start(scanDuration);
  }

    private static String join(String separator, List<String> input) {
        if (input == null || input.size() <= 0) return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.size(); i++) {
            sb.append(input.get(i));
            // if not the last item
            if (i != input.size() - 1) {
                sb.append(separator);
            }
        }
        return sb.toString();
    }

}

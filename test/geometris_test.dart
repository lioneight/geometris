import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:geometris/geometris.dart';

void main() {
  const MethodChannel channel = MethodChannel('geometris');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
//    expect(await Geometris.platformVersion, '42');
  });
}

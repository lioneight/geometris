import 'dart:async';

import 'package:flutter/services.dart';
import 'package:geometris/models.dart';

const EventChannel _stateEventChannel =
    EventChannel('lioneight.geometris/sensors/wherequbeState');

Stream<WherequbeStateEvent> _stateEvents;

/// A broadcast stream of events from the device accelerometer.
Stream<WherequbeStateEvent> get stateEvents {
  if (_stateEvents == null) {
    _stateEvents =
        _stateEventChannel.receiveBroadcastStream().map((dynamic event) {
          if(event is String){
            return _listToStateEvent(event);
          }
          return null;
    });
  }
  return _stateEvents;
}

WherequbeStateEvent _listToStateEvent(String stateEventResponse) {
  return WherequbeStateEvent(stateEventResponse, "");
}

const EventChannel _obdMeasurementEventChannel =
    EventChannel('lioneight.geometris/sensors/obdMeasurement');

Stream<String> _obdMeasurementStream;

Stream<String> get obdMeasurementStream {
  if (_obdMeasurementStream == null) {
    _obdMeasurementStream = _obdMeasurementEventChannel
        .receiveBroadcastStream()
        .map((dynamic event) {
      if(event is String){
        return event;
      }
      return null;
    });
  }
  return _obdMeasurementStream;
}

import 'dart:async';

import 'package:flutter/services.dart';
import 'package:geometris/models.dart';

const EventChannel _stateEventChannel = EventChannel('lioneight.geometris/sensors/wherequbeState');

List<Whereqube> _devicesFromResponse(String devices) {
  List<String> splited = devices.split(",");
  List<Whereqube> result = [];
  for (int i = 0; i < splited.length - 2; i = i + 3) {
    result.add(Whereqube(splited[i], splited[i + 1], splited[i + 2]));
  }
  return result;
}

class Geometris {
  static const MethodChannel _channel = const MethodChannel('lioneight.geometris');

  static Future<bool> wqaInitialize() async {
    return await _channel.invokeMethod('wqaInitialize');
  }

  static Future<bool> wherequbeServiceInitialize() async {
    return await _channel.invokeMethod('wherequbeServiceInitialize');
  }

  static Future<bool> wherequbeServiceDestroy() async {
    return await _channel.invokeMethod('wherequbeServiceDestroy');
  }

  static Future<bool> wherequbeServiceIsConnected() async {
    return await _channel.invokeMethod('wherequbeServiceIsConnected');
  }

  static Future<bool> wherequbeServiceConnect(String address) async {
    return await _channel.invokeMethod('wherequbeServiceConnect', <String, dynamic>{
      'address': address,
    });
  }

  static Future<bool> wherequbeServiceDisconnect() async {
    return await _channel.invokeMethod('wherequbeServiceDisconnect');
  }

  static Future<List<Whereqube>> wherequbeServiceScanDevices() async {
    String devicesString = await _channel.invokeMethod('wherequbeServiceScanDevices');
    return _devicesFromResponse(devicesString);
  }

  static Future<bool> wherequbeServiceStartTransmittingUnidentifiedDriverMessages() async {
    return await _channel.invokeMethod('wherequbeServiceStartTransmittingUnidentifiedDriverMessages');
  }

  static Future<bool> wherequbeServiceStopTransmittingUnidentifiedDriverMessages() async {
    return await _channel.invokeMethod('wherequbeServiceStopTransmittingUnidentifiedDriverMessages');
  }

  static Future<bool> wherequbeServicePurgeUnidentifiedDriverMessages() async {
    return await _channel.invokeMethod('wherequbeServicePurgeUnidentifiedDriverMessages');
  }
}

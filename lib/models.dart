class Whereqube {
  final String address;
  final String deviceName;
  final String deviceRssi;

  Whereqube(this.address, this.deviceName, this.deviceRssi);
}

class WherequbeStateEvent {
  WherequbeStateEvent(this.state, [this.wqErrorCode]);

  final String state;
  final String wqErrorCode;

  @override
  String toString() => '[WherequbeStateEvent (state: $state, y: $wqErrorCode)]';
}

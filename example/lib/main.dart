import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geometris/geometris.dart';
import 'package:geometris/geometris_sensors.dart';
import 'package:geometris/models.dart';
import 'package:location_permissions/location_permissions.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _message = 'Unknown';
  String _obdResponse = 'Unknown';
  String _deviceConnectionState = 'Unknown';
  List<Whereqube> devices = [];

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  @override
  Future<void> dispose() async {
    try {
      bool done = await Geometris.wherequbeServiceDestroy();
      print(done);
    } on PlatformException {}

    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    PermissionStatus permission = await LocationPermissions().requestPermissions();

    String message;
    try {
      bool done = await Geometris.wqaInitialize();
      print(done);
    } on PlatformException {
      message = 'Failed to wqaInitialize.';
      setState(() {
        _message = message;
      });
    }

    try {
      bool done = await Geometris.wherequbeServiceInitialize();
      print(done);
    } on PlatformException {
      message = 'Failed to wherequbeServiceInitialize.';
      setState(() {
        _message = message;
      });
    }

    stateEvents.listen((WherequbeStateEvent stateEvent) {
      print("demo state schanged: ${stateEvent.state}");
      setState(() {
        _deviceConnectionState = stateEvent.state;
      });
    });

    obdMeasurementStream.listen((String event) {
      setState(() {
        _obdResponse = event;
      });
    });

    try {
      bool connected = await Geometris.wherequbeServiceIsConnected();
      print(connected);
    } on PlatformException {
      message = 'Failed to wherequbeServiceIsConnected.';
      setState(() {
        _message = message;
      });
    }

    try {
      bool disconnected = await Geometris.wherequbeServiceDisconnect();
      print(disconnected);
    } on PlatformException {
      message = 'Failed to wherequbeServiceDisconnect.';
      setState(() {
        _message = message;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
            child: Column(
          children: <Widget>[
            Text('Last Mesage: $_message'),
            Text('Device connection state: $_deviceConnectionState'),
            RaisedButton(
              child: Text("Scan Devices"),
              onPressed: scanDevices,
            ),
            RaisedButton(
              child: Text("Disconnect device"),
              onPressed: Geometris.wherequbeServiceDisconnect,
            ),
            RaisedButton(
              child: Text("Receive Unidentified Driving"),
              onPressed: Geometris.wherequbeServiceStartTransmittingUnidentifiedDriverMessages,
            ),
            RaisedButton(
              child: Text("Purge UD events"),
              onPressed: Geometris.wherequbeServicePurgeUnidentifiedDriverMessages,
            ),
            Text('OBD response: $_obdResponse'),
            ListView.builder(
              shrinkWrap: true,
              itemCount: devices.length,
              itemBuilder: (BuildContext ctxt, int index) {
                return new Center(
                  child: RaisedButton(
                    child: Text(
                      devices[index].deviceName,
                    ),
                    onPressed: () async {
                      bool connected = await Geometris.wherequbeServiceConnect(devices[index].address);
                      print("Device connected: $connected");
                    },
                  ),
                );
              },
            )
          ],
        )),
      ),
    );
  }

  Future<void> scanDevices() async {
    setState(() {
      _message = "scanning";
    });

    List<Whereqube> wherequbes = await Geometris.wherequbeServiceScanDevices();
    print(devices);
    setState(() {
      _message = "scanned devices: ${wherequbes.length}";
      devices = wherequbes;
    });
  }
}
